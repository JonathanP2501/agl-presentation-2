package agl.tp5_mocks;

public enum NoticeStatus {
	newlyAdded, updated, nochange, notExisting;
}
